# Pmblues bot

Discord bot by Pmblue#1085 coded in Python

To Do:
```
Event: <event name>
Occurrence: <how often event occurs>
Info: <Description of event & information>
Tips: <Overall tips about events>
```

__*Possible Example:*__

**Event:**  Sample Strike

**Occurrence:**  Usually once a week

**Info:**  Mainly gives x technology and BPs, with some crafting boxes. Targets are called x, and range from 290-740 in level, and some targets are x. Targets can be found in x sector(s).

**Tips:**  Best targets to farm are x. Use x on x targets. x gives you intel.