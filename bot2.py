            try:
                choice = message.content[13:]   
                userReq = requests.get('https://api.kixeye.com:443/api/v2/user-games?gameId=386487958112133&playerId=' + choice)
                userDic = userReq.json()
                userDict = userDic[0]
                entryReq = requests.get('https://api.kixeye.com:443/api/v2/leaderboards/52678363c3b9ad7179000001/entries/{userDict["userId"]}')
                entryDic = entryReq.json()
                entryDict = entryDic[0]
                userAlias = entryDict["alias"]
                await message.channel.send(embed=discord.Embed(title=f'*{userAlias}*\'s Medal Stats',description=f'**Rank** {entryDict["rank"]}\n**Medal Count** {entryDict["value"]}'))
            except:
                embed=discord.Embed(title=f'INVALID SELECTION',description=f'The player ID you used was invalid!',color=discord.Color.red())
                embed = advertFooter(embed)
                await message.channel.send(embed=embed)
                return