import os
from datetime import datetime
import discord
from discord.ext import commands
import random
from random import randint
import asyncio
import re
import sys


client = discord.Client()

TOKEN = '*ha no*'


@client.event
async def on_ready():
    print(str(datetime.now()) + ': Bot is online!')
    await client.change_presence(activity=discord.Game(name=str('.help | Hello!')))
    return
    
@client.event
async def on_raw_reaction_add(payload):
    guild = client.get_guild(payload.guild_id)
    channel = client.get_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    guild = client.get_guild(payload.guild_id)
    member = guild.get_member(message.author.id)
    reamemb = guild.get_member(payload.user_id)
    if discord.utils.get(guild.roles, name='Officer') in reamemb.roles:
        if str(payload.emoji) == ":allyrole:":
            print('Ally Role')
            roleA = discord.utils.get(guild.roles, name='Ally/Friend')
            await member.add_roles(roleA)
            await message.remove_reaction(payload.emoji, member)
            await message.delete()
            return
        elif str(payload.emoji) == ":memberrole:":
            print('Member Role')
            roleM = discord.utils.get(guild.roles, name='Member')
            await member.add_roles(roleM)
            await message.remove_reaction(payload.emoji, member)
            await message.delete()
            return

    if str(payload.emoji) == u"\U00002705":
        channel = client.get_channel(payload.channel_id)
        guild = client.get_guild(payload.guild_id)
        member = guild.get_member(payload.user_id)
        message = await channel.fetch_message(payload.message_id)
        if message.id == 670022670410317836:
              guild = client.get_guild(617902434496282624)
              roleY = discord.utils.get(member.guild.roles, name='Event Notification')
              await member.add_roles(roleY)
              channel = client.get_channel(666285927957790743)
              return


@client.event
async def on_raw_reaction_remove(payload):
    if str(payload.emoji) == u"\U00002705":
        channel = client.get_channel(payload.channel_id)
        guild = client.get_guild(payload.guild_id)
        member = guild.get_member(payload.user_id)
        message = await channel.fetch_message(payload.message_id)
        if message.id == 670022670410317836:
              guild = client.get_guild(617902434496282624)
              roleY = discord.utils.get(member.guild.roles, name='Event Notification')
              await member.remove_roles(roleY)
              return

@client.event
async def on_message(message):
    msgCnt = str.lower(str(message.content))
    staffList = [312049173895839746,335493921608171520]
    if msgCnt.startswith('.hello'):
        msgAuthor = str(message.author)
        await message.channel.send('Hello, ' + msgAuthor)
        return

    if str.lower(message.content) == '.ping':
        msLatency = round(client.latency * 1000, 1)
        await message.channel.send('Pong! ' + str(msLatency) + 'ms')
        return
